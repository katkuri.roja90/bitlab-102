export default function (state = [], action) {
    switch (action.type) {
        case "STUDENT_DATA":
            return action.data;
        default:
            return state;
    }
}