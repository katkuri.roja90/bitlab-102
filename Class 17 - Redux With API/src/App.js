import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BooksData, MoviesData, StudentData, MoviesDataFromAPI } from './actions/actions.js';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  componentDidMount() {
    this.props.booksData();
    this.props.moviesData();
    this.props.studentData();
    this.props.moviesFromAPI();
  }

  render() {
    return (
      <div className="App">
        {this.props.books.map((e) => {
          return <h1>{e.name}</h1>;
        })}
        {this.props.movies.map((e) => {
          return <h1>{e.name}</h1>;
        })}
        {this.props.nameABCCAA.map((e) => {
          return <h1>{e.name}</h1>;
        })}
        {this.props.moviesMMM.map((e) => {
          return <div>
            <h1>{e.title}</h1>
            <div>Hello</div>
          </div>
        })}
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {
    books: state.books,
    movies: state.movies,
    nameABCCAA: state.nameAAA,
    moviesMMM: state.moviesFromAPI
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ booksData: BooksData, moviesData: MoviesData, studentData: StudentData, moviesFromAPI: MoviesDataFromAPI }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
