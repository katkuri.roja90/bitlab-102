import React from 'react';

let NotFound = () => {
    return <h2>Page Not Found</h2>
}

export default NotFound;