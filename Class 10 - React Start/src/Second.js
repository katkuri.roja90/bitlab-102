import React, { Component } from 'react';

class Second extends Component {
    render() {
        return <div>
            <h2>I'm a Class based component</h2>
            <h3>I'm {this.props.name}</h3>
        </div>
    }
}

export default Second;

// Class based component
// always has a render function in which you can return the html for the component.
// this inherits all the properties from the Component Class.