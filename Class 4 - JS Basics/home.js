var element = document.getElementById("myBlock");

element.innerHTML = "Hello There";
element.style.fontSize = "24px";

// number - integer, float 
// boolean , true or false
// string  , "asdda"
// arrays , [1,"2",true,5.23,"Noo"] // Array can have any datatype variables as it's elements.
// objects

var a = 10; // var can be used for any datatype
var b = "Hello";
var c = true;
var d = [1, "asas", 5.3, true];

console.log(e);
console.log("asas");
var e; // let can be used for any datatype
let n = "asas";

const m = 22; // const also can be used for any datatype, but we can't change it's value.
const y = "asas";

// m = 11; // This will throw the error Uncaught Error: Assignment to Constant Variable.

// camelCasing // starts with a lowercase letter and if there is a next word it's first letter will be uppercase ex: "hello there" -----> "helloThere"

printWords("Hello", "World");

function printWords(one, two) {
    console.log("Print " + one + " " + two);
}

var printWords2 = (abc, xyz) => {
    // document.getElementById("second").innerHTML = "Print " + abc + " " + xyz;
    console.log("Print " + abc + " " + xyz);
}
printWords2("Good", "Morning");

// undefined - Declared but not assigned, it is just a datatype
// not defined - Not Declared, it is an error, it stops you execution flow.

let asa = 10;
let bsa = 20;
console.log("Two Numbers addition: " + (asa + bsa))
// Arithmetic Operators ----> +,-,*,/


console.log(asa == bsa);

console.log(typeof (asa)); // number

// == checks only for value ex: 10 == "10"  true
// === checks for value and datatype // 10 === "10"  false

function btnClick() {
    console.log("Button Clicked");
}